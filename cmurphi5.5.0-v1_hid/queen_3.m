Type
	n: 0..3;/*代表行、列的数目*/
	state: -1..4;/*已放置的皇后数*/
	t: Array [ n ] Of 0..1;
	t_2: Array [ n ] Of t;
Var
	table: t_2;
	cnum: state;

Function search_r(i:n):state;/*同一行上棋子的数目*/
Var
tempi:n;
sum:state;
Begin
	tempi := i;
	sum := 0;
	for tempj:n do
		if table[tempi][tempj] = 1 then
			sum := sum+1;
		End;
	End;
	return sum;
End;
Function search_c(j:n):state;/*同一列上棋子的数目*/
Var
tempj:n;
sum:state;
Begin
	tempj := j;
	sum := 0;
	for tempi:n do
		if table[tempi][tempj] = 1 then
			sum := sum+1;
		End;
	End;
	return sum;
End;
Function search_dia(i:n;j:n):state;/*同一主副对角线上棋子的数目*/
Var
tempi:state;
tempj:state;
sum:state;
Begin
	sum := 0;
	tempj := j+1;
	tempi := i+1;
	While tempi <= 3 & tempj <= 3 Do
		if table[tempi][tempj] = 1 then
			sum := sum+1;
		End;
		tempj := tempj+1;
		tempi := tempi+1;
	End;
	tempj := j+1;
	tempi := i-1;
	While tempi >= 0 & tempj <= 3 Do
		if table[tempi][tempj] = 1 then
			sum := sum+1;
		End;
		tempj := tempj+1;
		tempi := tempi-1;
	End;
	tempj := j-1;
	tempi := i+1;
	While tempi <= 3 & tempj >= 0 Do
		if table[tempi][tempj] = 1 then
			sum := sum+1;
		End;
		tempj := tempj-1;
		tempi := tempi+1;
	End;
	tempj := j-1;
	tempi := i-1;
	While tempi >= 0 & tempj >= 0 Do
		if table[tempi][tempj] = 1 then
			sum := sum+1;
		End;
		tempj := tempj-1;
		tempi := tempi-1;
	End;
	return sum;
End;
Ruleset i:n;j:n Do
	Rule "set_queen"
		table[i][j] = 0 & cnum <4
	==>
	Begin
	  table[i][j] := 1;	
	  cnum := cnum+1;
	End;

	Rule "clear"
		cnum = 4
	==>
	Begin
	  clear table;
	  cnum := 0;
	End;
End;
Startstate 
Begin 
	clear table;
	cnum := 0;
End;	
Invariant 
	cnum = 4 ->	
	Exists a:n;b:n Do 
		table[a][b]=1 & (search_r(a)>1 | search_c(b)>1 | search_dia(a,b)>0)
	End;
		



import os
import pwd
import subprocess
from subprocess import PIPE
import hashlib

from pexpect import spawn, EOF, TIMEOUT
import re

class MurphiChk(object):
    def __init__(self, name, mu_path, mu_include, gxx_path, mu_file_dir,
        memory=4096, timeout=10000):
        super(MurphiChk, self).__init__()
        self.name = name #hashlib.md5(name).hexdigest()
        self.mu_path = mu_path
        self.mu_include = mu_include
        self.gxx_path = gxx_path
        self.mu_file_dir = mu_file_dir
        with open(name+'.m', "r") as f:
            mu_ctx=f.read()
        self.mu_ctx = mu_ctx
        self.memory = memory
        self.timeout = timeout

    def myCheck(self, checked):
        mu_file = self.gen_mu_file('')
        cpp_file = self.mu_compile(mu_file)
        exe_file = self.cpp_compile(cpp_file)
        res, checked=self.expect_read(exe_file, checked)
        print("res is {}".format(res))
        if res=='success':
            return None, checked
        else: 
            return re.sub('ABS_','',str(res)), checked

    def expect_read(self, filename, checked):
        #print(filename + '  -m %d -tv '%self.memory)
        process = spawn(command=filename + '  -m %d -tv '%self.memory, timeout=self.timeout)
        res = process.read()
        process.terminate(force=True)
        os.remove(os.path.join(self.mu_file_dir, self.name + '.m'))
        os.remove(os.path.join(self.mu_file_dir, self.name + '.cpp'))
        os.remove(os.path.join(self.mu_file_dir, self.name))
        digit='[0-9]'
        letter='[a-zA-Z]'
        letterdigit='[0-9a-zA-Z]'
        id=letter+'+(_'+letterdigit+"*)*"
        temp='ABS_'+id
        # print(str(res))
        fail=re.search('Invariant\s+".*?"\s+failed.', str(res))
        if fail is not None:
            # return re.search(temp,str(res))
            falserules = list(re.findall('Rule\s*(ABS_\w*).*?fired', str(res), re.S))
            print('falserules is {}'.format(falserules))
            # falserule = list(filter(lambda x : x not in checked and x != 'ABS_RecvReqE11' and x != 'ABS_SendGntE3' and x != 'ABS_RecvReqS12', falserules))
            falserule = list(filter(lambda x : x not in checked, falserules))
            assert falserule != [], "checked is {}".format(checked)
            checked.append(falserule[0])
            return falserule[0], checked
        else: 
            return'success', checked

    def check(self, inv):
        white_list = set([
            '(!((Sta.Dir.Dirty = FALSE) & (!(Sta.MemData = Sta.CurrData))))',
            '(!((!(Sta.HomeProc.CacheData = Sta.CurrData)) & (Sta.Dir.Local = TRUE)))',
            '(!((!(Sta.HomeProc.CacheData = Sta.CurrData)) & (Sta.HomeProc.CacheState = CACHE_E)))',
            '(!((!(Sta.HomeUniMsg.Data = Sta.CurrData)) & (Sta.HomeUniMsg.Cmd = UNI_Put)))',
            '(!((!(Sta.WbMsg.Data = Sta.CurrData)) & (Sta.WbMsg.Cmd = WB_Wb)))',
            '(!((!(Sta.ShWbMsg.Data = Sta.CurrData)) & (Sta.ShWbMsg.Cmd = SHWB_ShWb)))',
            '(!((!(Sta.HomeUniMsg.Data = Sta.CurrData)) & (Sta.HomeUniMsg.Cmd = UNI_PutX)))',
            '(!((!(Sta.Proc[1].CacheData = Sta.CurrData)) & (Sta.Proc[1].CacheState = CACHE_E)))',
            '(!((!(Sta.UniMsg[1].Data = Sta.CurrData)) & (Sta.UniMsg[1].Cmd = UNI_PutX)))',
        ])
        if inv in white_list:
            return 'true'
        mu_file = self.gen_mu_file(inv)
        cpp_file = self.mu_compile(mu_file)
        exe_file = self.cpp_compile(cpp_file)
        return 'false' if self.expect_fail(exe_file) else 'true'

    def gen_mu_file(self, inv):
        if not os.path.isdir(self.mu_file_dir):
            os.makedirs(self.mu_file_dir)
        filename = os.path.join(self.mu_file_dir, self.name + '.m')
        with open(filename, 'w') as f:
            f.write(self.mu_ctx)
            #f.write('\ninvariant "to check"\n%s;\n'%inv)
        return filename

    def mu_compile(self, filename):
        subprocess.call([self.mu_path, filename], stderr=PIPE, stdout=PIPE)
        return os.path.join(self.mu_file_dir, self.name + '.cpp')

    def cpp_compile(self, filename):
        exe_file = os.path.join(self.mu_file_dir, self.name)
        subprocess.call([self.gxx_path, filename, '-I', self.mu_include, '-o', exe_file],
            stderr=PIPE, stdout=PIPE)
        return exe_file

    def expect_fail(self, filename):
        process = spawn(filename + ' -pn -m %d '%self.memory)
        res = process.expect(['Invariant\s+".*?"\s+failed.', EOF, TIMEOUT], timeout=self.timeout)
        process.terminate(force=True)
        os.remove(os.path.join(self.mu_file_dir, self.name + '.m'))
        os.remove(os.path.join(self.mu_file_dir, self.name + '.cpp'))
        os.remove(os.path.join(self.mu_file_dir, self.name))
        return res == 0




if __name__ == '__main__':
    ctx = ''
    with open('mutualEx.m', 'r') as f:
        ctx = f.read()
    mu = MurphiChk('output1',
        '/Users/lyj238/Downloads/cmurphi5.4.9/src/mu',
        '/Users/lyj238/Downloads/cmurphi5.4.9/include',
        '/usr/bin/g++',
        '/tmp/cmurphi/'
        )
    print (mu.check('!(n[1] = C & n[2] = C)')) 
    print(mu.myCheck())


# import re
# import os
# import sys
# import subprocess

# class MurphiChk(object):
#     def __init__(self, name, mu_path, mu_include, data_dir, 
#         memory=1024, timeout=600):
#         super(MurphiChk, self).__init__()
#         self.name = name #hashlib.md5(name).hexdigest()
#         self.data_dir = data_dir
#         self.mu_path = mu_path
#         self.mu_include = mu_include
#         self.memory = memory
#         self.timeout = timeout

#     def myCheck(self):
#         print('[call murphi]compile murphi to cpp....')
#         # print(self.mu_path, self.name, self)
#         command1 = "{0} {2}/{1}.m".format(self.mu_path, self.name, self.data_dir)
#         command2 = "g++ -ggdb -o {0}/{1}.o {0}/{1}.cpp -I {2} -lm".format(self.data_dir, self.name,
#                                                                                         self.mu_include)
#         command3 = "{0}/{1}.o -m{2} -pn".format(self.data_dir, self.name, self.memory)

#         print('command1 = {}'.format(command1))
#         print('command2 = {}'.format(command2))
#         print('command3 = {}'.format(command3))

#         print('compile murphi file to cpp....')

#         process1 = subprocess.Popen(command1,
#                                     shell=True,
#                                     stdout=subprocess.PIPE,
#                                     stderr=subprocess.PIPE)
#         (stdout, stderr) = process1.communicate()
#         if not re.search(r'Code generated', stdout.decode('utf-8')):
#             print('Wrong', stderr.decode('utf-8'))
#             raise ValueError
#             sys.exit()
#         else:
#             print('Code generated')

#         print('compile .cpp to .o file....')
#         try:
#             process2 = subprocess.Popen(command2, shell=True,
#                                         stdout=subprocess.PIPE,
#                                         stderr=subprocess.PIPE)
#             (stdout, stderr) = process2.communicate()
#         except:
#             raise ValueError(stdout.decode('utf-8'))

#         print('running .o file....')

#         try:
#             process = subprocess.Popen(command3, shell=True,
#                                     stdout=subprocess.PIPE,
#                                     stderr=subprocess.PIPE)

#             (stdoutdata, stderrdata) = process.communicate()
#         except:
#             raise ValueError(stdout.decode('utf-8'))
#         else:
#             pattern_counter = re.compile(r'Invariant\s"(\w+).*"\sfailed.')
#             counter_ex = re.findall(pattern_counter, stdoutdata.decode('utf-8'))
#             if len(counter_ex):
#                 print("%s failed!" % ','.join(counter_ex))

#             os.remove('{0}/{1}.cpp'.format(self.data_dir, self.name))
#             # os.remove('{0}/{1}.o'.format(self.data_dir, self.name))

#         return False if len(counter_ex) else True
